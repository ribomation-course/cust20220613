#pragma once

#include <iostream>
#include <syncstream>
#include <array>
#include <mutex>
#include <condition_variable>
#include "concurrent.hxx"

namespace ribomation::mq {
    namespace co = ribomation::concurrent;
    using std::array;
    using std::cerr;
    using std::osyncstream;
    using co::mutex;
    using co::unique_lock;
    using co::condition_variable;

    template<typename MsgType, unsigned CAPACITY, bool VERBOSE = false>
    class MessageQueue {
        array<MsgType, CAPACITY> queue;
        unsigned                 putIx        = 0;
        unsigned                 getIx        = 0;
        unsigned                 num_messages = 0;

        mutex              exclusive;
        condition_variable not_full;
        condition_variable not_empty;

    public:
        void put(MsgType msg) {
            auto g = unique_lock{exclusive};
            not_full.wait(g, [this] { return !full(); });

            if constexpr (VERBOSE) {
                osyncstream{cerr} << "[mq] put(" << msg << ")\n";
            }
            queue[putIx] = msg;
            putIx = (putIx + 1) % CAPACITY;
            ++num_messages;

            not_empty.notify_all();
        }

        MsgType get() {
            auto g = unique_lock{exclusive};
            not_empty.wait(g, [this] { return !empty(); });

            if constexpr (VERBOSE) {
                osyncstream{cerr} << "[mq] get() -> " << queue[getIx] << "\n";
            }
            MsgType msg = queue[getIx];
            getIx = (getIx + 1) % CAPACITY;
            --num_messages;

            not_full.notify_all();
            return msg;
        }

        auto size() const { return num_messages; }

        bool full() const { return num_messages == CAPACITY; }

        bool empty() const { return num_messages == 0; }

        MessageQueue() = default;
        MessageQueue(const MessageQueue<MsgType, CAPACITY>&) = delete;
        MessageQueue& operator=(const MessageQueue<MsgType, CAPACITY>&) = delete;
    };

}
