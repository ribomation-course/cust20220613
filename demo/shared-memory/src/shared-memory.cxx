#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <cmath>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "shared-memory.hxx"

namespace {

    void sysFail(const std::string& func) {
        std::string msg = func + "() failed: " + ::strerror(errno);
        throw std::runtime_error(msg);
    }

    unsigned adjustSize(size_t size) {
        const double PS = ::getpagesize();
        return static_cast<unsigned>(::ceil(size / PS) * PS);
    }

    std::string adjustName(const std::string& name) {
        if (name[0] != '/') {
            const_cast<std::string&>(name) = '/' + name;
        }
        return name;
    }

    void* add(void* base, size_t offset) {
        return reinterpret_cast<char*>(base) + offset;
    }

}

namespace ribomation::shm {

    SharedMemory::SharedMemory(size_t size_, void* addr, const std::string& name_)
            : name{adjustName(name_)}, size{adjustSize(size_)} {
        int fd = shm_open(name.c_str(), O_RDWR | O_CREAT | O_TRUNC, 0660);
        if (fd < 0) sysFail("shm_open");

        int rc = ftruncate(fd, size);
        if (rc < 0) sysFail("ftruncate");

        void* start = mmap(addr, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (start == MAP_FAILED) sysFail("mmap");
        close(fd);

        SharedMemory::begin = start;
        SharedMemory::end   = add(start, size);
        SharedMemory::next  = start;
    }

    SharedMemory::~SharedMemory() {
        munmap(const_cast<void*>(begin), size);
        shm_unlink(name.c_str());
    }

    SharedMemory::SharedMemory(SharedMemory&& rhs) noexcept
            : name{rhs.name}, size{rhs.size},
              begin{rhs.begin}, end{rhs.end}, next{rhs.next} {
        rhs.next = nullptr;
    }

    void* SharedMemory::allocateBytes(size_t num_bytes) {
        if (next == nullptr) {
            throw std::domain_error{"in moved-from state"};
        }
        void* addr = next;
        if (add(next, num_bytes) > end) {
            throw std::overflow_error("shared-memory overflow");
        }

        next = add(next, num_bytes);
        return addr;
    }

}

