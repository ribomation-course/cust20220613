#pragma once

#include <memory_resource>
#include "concurrent.hxx"

namespace ribomation::pmr {
    namespace pmr = std::pmr;
    namespace rm = ribomation::concurrent;

    class shm_pool_resource : public pmr::unsynchronized_pool_resource {
        mutable rm::mutex entryLock;
    public:
        explicit shm_pool_resource(pmr::memory_resource* upstream) : pmr::unsynchronized_pool_resource(upstream) {}

    protected:
        void* do_allocate(size_t __bytes, size_t __alignment) override {
            auto g = rm::unique_lock{entryLock};
            return unsynchronized_pool_resource::do_allocate(__bytes, __alignment);
        }

        void do_deallocate(void* __p, size_t __bytes, size_t __alignment) override {
            auto g = rm::unique_lock{entryLock};
            unsynchronized_pool_resource::do_deallocate(__p, __bytes, __alignment);
        }

        bool do_is_equal(const memory_resource& __other) const noexcept override {
            auto g = rm::unique_lock{entryLock};
            return unsynchronized_pool_resource::do_is_equal(__other);
        }
    };

}

