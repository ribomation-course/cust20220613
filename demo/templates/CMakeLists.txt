cmake_minimum_required(VERSION 3.10)
project(templates)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors -Wno-unused-but-set-variable -Wno-unused-but-set-parameter)

add_executable(simple-fold simple-fold.cxx)
add_executable(csv csv.cxx)
add_executable(maclaurin-series maclaurin-series.cxx)


