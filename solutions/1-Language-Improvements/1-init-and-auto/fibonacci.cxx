#include <iostream>
#include <map>

using std::cout;

auto fib(unsigned n) {
    if (n == 0) return 0UL;
    if (n == 1) return 1UL;
    return fib(n - 2) + fib(n - 1);
}

struct Pair {
    unsigned arg;
    unsigned long res;
};

auto compute(unsigned n) {
    return Pair{n, fib(n)};
}

auto table(unsigned n) /*-> std::map<unsigned, unsigned>*/ {
    auto tbl = std::map<unsigned, unsigned>{};
    
    for (auto k = 1U; k <= n; ++k) {
        auto [a, r] = compute(k);
        tbl[a] = r;
    }
    return tbl;
}

int main() {
    auto const N = 40;
    {
        auto result = fib(N);
        cout << "fib(" << N << ") = " << result << "\n";
    }
    {
        auto [arg, result] = compute(N);
        cout << "fib(" << arg << ") = " << result << "\n";
    }
    {
        for (auto [arg, result] : table(N))
            cout << "fib(" << arg << ") = " << result << "\n";
    }
}
