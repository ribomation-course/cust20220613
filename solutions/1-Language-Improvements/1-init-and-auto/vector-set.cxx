#include <iostream>
#include <vector>
#include <string>
#include <set>

using std::vector;
using std::set;
using std::string;
using std::cout;
using namespace std::literals::string_literals;

int main() {
    {
        auto words = vector<string>{
                "C++"s, "is"s, "great"s, "!!"s
        };
        for (auto const& w: words) cout << w << " ";
        cout << "\n";
    }
    {
        auto words = set<string>{
                "C++"s, "is"s, "great"s, "!!"s
        };
        for (auto const& w: words) cout << w << " ";
        cout << "\n";
    }
}
