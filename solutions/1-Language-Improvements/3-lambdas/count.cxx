#include <iostream>
#include <functional>

using std::cout;
using std::function;
using std::begin;
using std::end;
using std::size;

unsigned count_if(int* first, int* last, function<bool(int)> predicate) {
    unsigned cnt = 0;
    for (; first != last; ++first) if (predicate(*first)) ++cnt;
    return cnt;
}

int main() {
    int  arr[]  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto isOdd  = [](auto x) { return x % 2 == 1; };
    auto result = count_if(begin(arr), end(arr), isOdd);
    cout << "# odd: " << result << "\n";
}
