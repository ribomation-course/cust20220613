#include <iostream>
#include <algorithm>

using std::cout;
using std::for_each;
using std::transform;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N         = std::size(numbers);

    auto print = [](auto x) { cout << x << " "; };
    for_each(numbers, numbers + N, print);
    cout << "\n";
    auto factor = 42;
    transform(numbers, numbers + N, numbers, [factor](auto x) { return x * factor; });
    for_each(std::begin(numbers), std::end(numbers), print);
    cout << "\n";
}
