#include <iostream>
#include <string>

using std::cout;
using std::string;
using namespace std::literals::string_literals;

template<typename T>
T maximum(T a, T b) { return a >= b ? a : b; }

template<typename T>
T maximum(T a, T b, T c) { return maximum(maximum(a, b), c); }

int main() {
    {
        auto x = 10, y = 20, z = 30;
        cout << "max: " << maximum(x, y, z) << "\n";
    }
    {
        auto x = "hello"s, y = "howdy"s, z = "hi"s;
        cout << "max: \"" << maximum(x, y, z) << "\"\n";
    }
}
