#include <iostream>
using std::cout;

struct NoCopy {
    int value = 42;
    NoCopy(int v) : value{v} {}
    NoCopy() = default;
    NoCopy(NoCopy const&) = delete;
    auto operator=(NoCopy const&) -> NoCopy& = delete;
};

void fn1(NoCopy obj) {
    obj.value++;
}

void fn2(NoCopy& obj) {
    obj.value++;
}

int main() {
    auto o = NoCopy{17};
    // fn1(o); error: use of deleted function ‘NoCopy::NoCopy(const NoCopy&)’
    fn2(o);
    cout << "o.value: " << o.value << "\n";

    auto o2 = NoCopy{};
    // o2 = o; error: use of deleted function ‘NoCopy& NoCopy::operator=(const NoCopy&)’
    cout << "o2.value: " << o2.value << "\n";
}
