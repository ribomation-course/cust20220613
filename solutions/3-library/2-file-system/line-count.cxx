#include <iostream>
#include <fstream>
#include <filesystem>
#include <stdexcept>
#include <unordered_set>
#include <string>

namespace fs = std::filesystem;
using namespace std::string_literals;
using std::cout;
using std::string;

unsigned line_count(fs::path const& path);
unsigned process_entry(fs::directory_entry const& entry);

int main(int argc, char** argv) {
    auto dir = argc == 1 ? fs::current_path() : fs::path{argv[1]};
    if (!fs::is_directory(dir))
        throw std::invalid_argument{"not a dir: "s + dir.string()};

    auto totalCnt = 0U;
    for (auto const& entry: fs::recursive_directory_iterator{dir}) {
        totalCnt += process_entry(entry);
    }
    cout << "TOTAL: " << totalCnt << " lines\n";
}

unsigned process_entry(fs::directory_entry const& entry) {
    static auto EXTS = std::unordered_set<string>{".txt"s, ".cxx"s, ".hxx"s};
    const auto& filename = entry.path();
    if (fs::is_regular_file(filename)) {
        if (EXTS.count(filename.extension()) == 1) {
            auto cnt = line_count(filename);
            cout << filename << ": " << cnt << " lines\n";
            return cnt;
        }
    }
    return 0;
}

unsigned line_count(fs::path const& path) {
    auto f = std::ifstream{path};
    if (!f) throw std::runtime_error{"cannot open for read: "s + path.string()};

    auto        cnt = 0U;
    for (string line; std::getline(f, line);) ++cnt;
    return cnt;
}
