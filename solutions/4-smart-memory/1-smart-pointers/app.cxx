#include <iostream>
#include <string>
#include <memory>

using std::cout;
using std::string;

struct Person {
    string   name = "no name";
    unsigned age  = 18;

    Person() {
        cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
    }

    Person(string const& n, unsigned a) : name{n}, age{a} {
        cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
    }

    ~Person() {
        cout << "~Person{} @ " << this << "\n";
    }
};

void fn(std::unique_ptr<Person> q) {
    q->age++;
    cout << "[fn] q: " << q->name << ", " << q->age << "\n";
}

int main() {
    auto p1 = std::make_unique<Person>("Nisse Hult", 42);
    cout << "p1: " << p1->name << ", " << p1->age << "\n";
    fn( std::move(p1) );
    cout << "[main] exit\n";
}
