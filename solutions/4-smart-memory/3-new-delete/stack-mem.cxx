#include <iostream>
#include <string>
#include <alloca.h>
using std::cout;

struct stk_mem_t {};
extern const stk_mem_t stk_mem;

__attribute__ ((always_inline))
inline void* operator new(size_t numBytes, stk_mem_t) {
    return ::alloca(numBytes);
}

struct Link {
    unsigned value;
    Link* next;

    Link(unsigned v, Link* n) : value{v}, next{n} {
        cout << "Link{" << value << ", " << next << "} @ " << this << "\n";
    }
};

void run(unsigned N) {
    Link* head = nullptr;
    for (auto k = N; k >0; --k) head = new (stk_mem) Link{k, head};
    for (auto n = head; n != nullptr; n = n->next) cout << n->value << " ";
    cout << "\n";
}

int main(int argc, char** argv) {
    run(5);
    run(15);
    if (argc == 2) run(std::stoi(argv[1]));
}
